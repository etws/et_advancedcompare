== Extension links
Permanent link: http://shop.etwebsolutions.com/eng/et-advancedcompare.html
Support link: http://support.etwebsolutions.com/projects/et-advancedcompare/roadmap

== Short Description
Extension allows to add products to compare without page reloading (AJAX),
improves visualization of results and allows to switch compare off on your website.


== Version Compatibility
Magento CE:
1.3.х (tested in 1.3.2.4)
1.4.x (tested in 1.4.1.1)
1.5.x (tested in 1.5.0.1 1.5.1.0)
1.6.x (tested in 1.6.1.0)
1.7.x (tested in 1.7.0.2)
1.8.x (tested in 1.8.1.0)
1.9.x (tested in 1.9.0.1)

== Installation
* Disable compilation if it is enabled (System -> Tools -> Compilation)
* Disable cache if it is enabled (System -> Cache Management)
* Download the extension or install the extension from Magento Connect
* If you have downloaded it, copy all files from the "install" folder to
  the Magento root folder - where your index.php is
* Log out from the admin panel
* Log in to the admin panel with your login and password
* Set extension's parameters (System -> Configuration -> ET EXTENSIONS -> Advanced Compare)
* Run the compilation process and enable cache if needed
* Rebuild Flat Catalog Product index (relevant for Magento 1.3.x)


== Information for Developers
Code Encryption / Obfuscation
    Extensions developed by ETWS are not encrypted (no IonCube, Zend Guard, etc.).
    100% of the source code is open source and not a single line of code is obfuscated / encrypted.
    
Observers / Events
    The extension listens for the following events, and uses them to add its functionality to Magento.
    Area: global - Event: controller_action_layout_generate_blocks_after

Class Rewrites
    Helper:	Mage_Catalog_Helper_Product_Compare => ET_AdvancedCompare_Helper_Data
    Models: Mage_Catalog_Model_Resource_Product_Compare_Item_Collection => ET_AdvancedCompare_Model_Resource_Eav_Mysql4_Product_Compare_Item_Collection
    Controller: Mage_Catalog_Product_CompareController => ET_AdvancedCompare_IndexController

Template Rewrites
    catalog/product/compare/list.phtml

Database changes
Add Attribute
    remove_compare_link to catalog_product

External Libraries
    don't use libraries